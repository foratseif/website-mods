(function(){
    // variable definitions
    var murl = "https://www.facebook.com/messages"; // redirect to
    var furl = "http://plexnexus.com";              // homebutton redirect, could be "#"
    var lurl = "https://www.facebook.com/l.php?u="; // the url for links in facebook chats
    var bypass_keyword = "gogi";                    // keyword in the url that stops the redirect

    // redirect to messages page
    if( window.location.href.indexOf(murl) != 0 &&
        window.location.href.indexOf(lurl) != 0 &&
        window.location.href.indexOf(bypass_keyword) == -1 )
    {
        window.location = murl;
    }

    // * block the home buttons *

    // classes of all the homebuttons (for querySelector)
    home_btns = "._19eb, ._2s25";

    // vars to keep track of how many btns are blocked, and how many to block
    links_counter = 0;
    links_counter_max = home_btns.split(',').length;

    // function may run more than once
    function remove_home_links(){
        // select home buttons
        var x = document.querySelectorAll(home_btns);
        links_counter += x.length;

        // loop through selected buttons, and change the href
        for(var i=0; i<x.length; i++)
            x[i].setAttribute("href", furl);

        // run function again (if not all specified buttons were modified)
        if(links_counter < links_counter_max)
            setTimeout(function(){ remove_home_links(); }, 500);
    }

    // start the function.
    remove_home_links();
})();
