// ====== AD SKIPPING AND MUTING ======
(function(){
    function get_mute_btn(){
        return document.getElementsByClassName("ytp-mute-button")[0];
    }

    function is_muted(){
        return get_mute_btn().getAttribute("Title") == "Unmute";
    }

    function is_ad(){
        return document.getElementsByClassName("ad-interrupting").length > 0;
    }

    function is_skippable(){
        var con = document.getElementsByClassName("videoAdUiSkipContainer");
        return (con.length && con[0].style.opacity != "0");
    }

    function get_ad_skip_btn(){
        return document.getElementsByClassName('videoAdUiSkipButton')[0];
    }

    //variable for keeping track of muting, -1 = not set, 0 = no mute, 1 = muted
    var pre_ad_mute = -1;

    window.forat_ad_skip = setInterval(function(){
         // check if there is no ad
         if( !is_ad() ){
             // unmute the video (if muted)
             if(pre_ad_mute == 0 && is_muted()){
                 get_mute_btn().click();
            }

            // unset pre_ad_mute
            pre_ad_mute = -1;

            // stop here since there is no ad to skip/mute
            return;
         }

         // check if ad is skippable
         if( is_skippable() ){
            // skip ad
            get_ad_skip_btn().click();
         }

         // mute the ad (if unmuted)
         if( pre_ad_mute == -1 ){
            //set pre_ad_mute
            pre_ad_mute = is_muted() ? 1 : 0;

            //mute ad (if not muted)
            if(pre_ad_mute == 0){
                 get_mute_btn().click();
            }
         }

    }, 250);
})();
