// ====== AD SKIPPING ======
(function(){
    function is_skippable(){
        var con = document.getElementsByClassName("videoAdUiSkipContainer");
        return (con.length && con[0].style.opacity != "0");
    }

    function get_ad_skip_btn(){
        return document.getElementsByClassName('videoAdUiSkipButton')[0];
    }

    window.forat_ad_skip = setInterval(function(){

        // check if ad is skippable
        if( is_skippable() ){
            // skip ad
            get_ad_skip_btn().click();
        }
    }, 250);
})();
