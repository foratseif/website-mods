/* fill screen plugin */
(function(){
    
    function fill_screen(){
    
        // check for #fill parameter
        if( window.location.href.indexOf("#fill") > -1){
    
            var add_fill_class = function(){
                var v = document.querySelectorAll("#player-container.ytd-watch");
                if(v.length){
                    var class_attr = v[0].getAttribute("class");
                    v[0].setAttribute("class", class_attr+" forat_fillscreen");
                }else{
                    setTimeout(function(){
                        fill_screen();
                    }, 500);
                }
            }
    
            add_fill_class();
        }
        
    }
    
    fill_screen();
})();
